//
//  PSComboBox.h
//  ListDemo
//
//  Created by Peate on 15/5/15.
//  Copyright (c) 2015年 Peate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSComboBox : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIImageView *img;
@property (nonatomic, assign) NSInteger cellHeight;
@property (nonatomic, strong) NSArray  *dataSource;
@property (nonatomic, assign) void (^block)(NSInteger row);


- (instancetype)initWithFrame:(CGRect)frame;
- (void)setDataSource:(NSArray *)dataSource;

@end
