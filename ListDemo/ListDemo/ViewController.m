//
//  ViewController.m
//  ListDemo
//
//  Created by Peate on 15/5/15.
//  Copyright (c) 2015年 Peate. All rights reserved.
//

#import "ViewController.h"
#import "PSComboBox.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    PSComboBox *combobox = [[PSComboBox alloc] initWithFrame:CGRectMake(100, 100, 100, 50)];
    combobox.title.text = @"哇哈哈";
    combobox.layer.borderWidth = 1;
    combobox.dataSource = @[@"我的很烦社科系就饿了",@"我就饿机械",@"小尅肌肤"];
    combobox.block = ^(NSInteger row){
    
        NSLog(@"row %ld",row);
    };
    [self.view addSubview:combobox];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
