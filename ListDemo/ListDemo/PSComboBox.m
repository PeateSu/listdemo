//
//  PSComboBox.m
//  ListDemo
//
//  Created by Peate on 15/5/15.
//  Copyright (c) 2015年 Peate. All rights reserved.
//

#import "PSComboBox.h"

@interface PSComboBox ()

@property (nonatomic, strong) UIView *bg;

@end

@implementation PSComboBox

- (instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        [self initializeUserInterface];
    }
    return self;
}

- (void)initializeUserInterface{

    //设置title
     self.title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.frame.size.width - 30, self.frame.size.height - 10)];
    self.title.textAlignment = 1;
    self.title.textColor = [UIColor blackColor];
//    self.title.backgroundColor = [UIColor yellowColor];
    self.title.center = CGPointMake(self.title.center.x, self.frame.size.height / 2.0);
    [self addSubview:self.title];
    //设置图片
    self.img = [[UIImageView alloc] init];
    self.img.bounds = CGRectMake(0, 0, 10, 10);
    self.img.center = CGPointMake(self.title.bounds.size.width + 20, self.center.y);
    [self addSubview:self.img];
    //设置点击时间
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTheView)];
    [self addGestureRecognizer:tap];
}

- (void)tapTheView{
    
    self.title.text = @"nih ";
    //获取当前顶层视图
    UIView *parentView = nil;
    NSArray *windows = [UIApplication sharedApplication].windows;
    UIWindow *window = [windows objectAtIndex:0];
    if (window.subviews.count > 0) {
        parentView = [window.subviews objectAtIndex:0];
    }
    //添加背景
    self.bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.bg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [parentView addSubview:self.bg];
    //添加手势
    UIButton *tap =[UIButton buttonWithType:UIButtonTypeCustom];
    tap.frame = self.bg.bounds;
    [tap addTarget:self action:@selector(dismissComboBox) forControlEvents:UIControlEventTouchUpInside];
    [self.bg addSubview:tap];
    //获取table的frame
    CGSize size = CGSizeMake([UIScreen mainScreen].bounds.size.width, 0);
    float Maxwidth = 0;
    for (NSString *str in self.dataSource) {
      CGRect rect = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
        if (Maxwidth < rect.size.width) {
            Maxwidth = rect.size.width;
        }else{
            continue;
        }
    }
    //设置table
    UITableView *table = [[UITableView alloc] init];
    table.frame = CGRectMake(10, self.frame.origin.y + self.frame.size.height+30, Maxwidth, self.cellHeight > 0 ? self.dataSource.count * self.cellHeight : self.dataSource.count * 44);
    table.delegate = self;
    table.dataSource = self;
    [self.bg addSubview:table];
}

- (void)dismissComboBox{
    [self.bg removeFromSuperview];
}

#pragma mark ------------------- UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return self.cellHeight > 0 ? self.cellHeight : 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"cellComboBox";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.textLabel.text = self.dataSource[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.title.text = self.dataSource[indexPath.row];
    self.block(indexPath.row);
    [self dismissComboBox];
}

@end
